signature SIGNATURE = sig
    type symbol
    val compare : symbol * symbol -> order
    val arity : symbol -> int
end


(* Basic types are booleans and integers *)
structure TypeSig : SIGNATURE = struct

    datatype symbol = INT
		    | BOOL
		    | ARROW

    fun arity INT   = 0
      | arity BOOL  = 0
      | arity ARROW = 2

    fun toInt INT   = 0
      | toInt BOOL  = 1
      | toInt ARROW = 2

    fun compare (s1, s2) = Int.compare (toInt s1, toInt s2)

end


signature VAR = sig
    type var
    type ord_key = var
    val compare : var * var -> order
end

functor Unify (
    structure S : SIGNATURE
    structure V : VAR
) = struct

   datatype term = Var of V.var
	         | Apply of S.symbol * term list


   structure VarMap = RedBlackMapFn(V)
   (*
    A telescope {x₁ <- t₁ ...., xₙ <- tₙ } is captured

   as a map between Atoms to terms.

   *)

   type telescope = term VarMap.map
   type equation  = term * term

   fun checkRecursion (tel : telescope) (x : V.var)(t : term) = false

   fun unify (tel : telescope) (sEQt : equation) : telescope =
       case sEQt of
	   (Var x , t    )                          => unifyVar tel x t
	 | (s     , Var y)                          => unifyVar tel y s
	 | (Apply (f, fargs) , Apply (g, gargs) )   => VarMap.empty
   and unifyVar (tel : telescope) (x : V.var) (t : term) : telescope = VarMap.empty

   and unifyList (tel : telescope) (eqns : equation list)  : telescope = VarMap.empty

end
